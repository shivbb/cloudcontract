package com.shiv.cloudcontract;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CloudcontractApplication {

	public static void main(String[] args) {
		SpringApplication.run(CloudcontractApplication.class, args);
	}

}
