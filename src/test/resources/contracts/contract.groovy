import org.springframework.cloud.contract.spec.Contract
Contract.make {
    description "should return hello"
    request{
        method GET()
        url("hello") {
//            queryParameters {
//                parameter("number", "2")
//            }
        }
    }
    response {
        body("Hello....")
        status 200
    }
}